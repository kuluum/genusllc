package testPackage;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 
 * Time stamp getin/parser class.
 */
public abstract class TimeStamp {

	/**
	 * @Field format date format
	 */
	private static String format = "yyyyMMddHHmmss";
	
	/**
	 * Returns timestamp by format
	 * @return timestamp string
	 */
	public static String getTimeStamp() {
		
		Calendar calendar = Calendar.getInstance();
		Date date = calendar.getTime();  
	    DateFormat dateFormat = new SimpleDateFormat(format); 
	    
	    return dateFormat.format(date);  
	}
	
	/**
	 * Parses date string by format
	 * @param dateStr date string
	 * @return readable date string
	 */
	public static String parse(String dateStr) {
			
		    DateFormat dateFormat = new SimpleDateFormat(format); 
		    Date date;
			
		    try {
				date = dateFormat.parse(dateStr);
			} catch (ParseException e) {
				e.printStackTrace();
				return "Date error";
			}
		    
		    return date.toString();  
		}

}
