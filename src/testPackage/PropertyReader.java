package testPackage;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 * Property file reader.
 */

public class PropertyReader {
	
	/**
	 * @Field fileName name of file with properties
	 */
	private String fileName = ".properties";
	
	/**
	 * Empty class constructor. Do nothing.
	 */
	public PropertyReader() {}
	
	/**
	 * Class constructor. Change standard property file name
	 * @param fileName name of file
	 */
	public PropertyReader(String fileName) {
		this.fileName = fileName;
	}
	
	/**
	 * Returns property value by it name
	 * @param propName name of property
	 * @return value of property
	 * @throws Exception
	 */
	private String getProp(String propName) throws Exception {
		
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		String line;
		String prop;
		
		while ( ((line = br.readLine()) != null) && (!line.startsWith(propName)) ) {  ;	} // Read file until meeting gentle param name suffix
		
		if(line == null){
			prop = null;
		}
		else{
			String[] splited = line.split("=");
			prop = splited[1];
		}
		
		br.close();
		
		return prop;
	}
	
	/**
	 * Returns "URL" property
	 * @return URL string
	 * @throws Exception
	 */
	public String getURL() throws Exception{
		return getProp("URL");
	}
	
	/**
	 * Returns KEY property
	 * @return KEY string
	 * @throws Exception
	 */
	public String getKey() throws Exception{
		return getProp("KEY");
	}
	
}
