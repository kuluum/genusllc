package testPackage;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

/**
 * Crypter Class.
 */
public class Crypter {
	/**
	 * @transformation encryption type
	 */
	private String transformation = "DES";
			
	private Cipher ecipher;
	private Cipher dcipher;
	
	/**
	 * Class constructor
	 * @param key enctyption/decryption key
	 * @throws Exception in case of wrong key length ( <8 )
	 */
	public Crypter(String key) throws Exception {	  
		
		if(key.length() < 8 ){
			throw new RuntimeException("Key length must be >= 8");
		}
		
		DESKeySpec dks = new DESKeySpec(key.getBytes());
		SecretKeyFactory skf = SecretKeyFactory.getInstance(transformation);
		SecretKey desKey = skf.generateSecret(dks);
		
		ecipher = Cipher.getInstance(transformation);
		dcipher = Cipher.getInstance(transformation);
	    ecipher.init(Cipher.ENCRYPT_MODE, desKey);
	    dcipher.init(Cipher.DECRYPT_MODE, desKey);
	  }

	/**
	 * Encrypts bytes array with @transformation encryption
	 * @param inpBytes input bytes array to encrypt
	 * @return encrypted beytes array
	 * @throws Exception
	 */
	 public byte[] encrypt(byte[] inpBytes) throws Exception {
		 return ecipher.doFinal(inpBytes);
	 }
	 
	 /**
	  * Decrypts bytes array with @transformation encryption
	  * @param inpBytes input bytes array to decrypt
	  * @return decrypted bytes array
	  * @throws Exception
	  */
	 public byte[] decrypt(byte[] inpBytes) throws Exception {
		return dcipher.doFinal(inpBytes);
	}
		
}
