package testPackage;

/**
 * Hex string - byte converter.
 */

public class HexConverter {

	/**
	 * Convert array of byte to hex string
	 * @param a byte array
	 * @return hex string
	 */
	public static String byteArrayToHex(byte[] a) {
		   StringBuilder sb = new StringBuilder(a.length * 2);
		   for(byte b: a)
		      sb.append(String.format("%02x", b & 0xff));
		   return sb.toString();
		}
	
	/**
	 * Convert hex string to byte array
	 * @param s hex string
	 * @return byte array
	 */
	public static byte[] HexToByteArray(String s) {
		   return  new java.math.BigInteger(s,16).toByteArray();
		}
}
