package testPackage;

import java.io.*;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.*;

/**
 * Decryption test servlet.
 */
@WebServlet("/decryptiontest") //URL path
public class DecryptionTest extends HttpServlet {
		
	/**
	 * @Field serialVersionUID Dunow forwhat.
	 * @Field propFileName path to the file with properties
	 * @Field paramName name of param in get request
	 * @Field baseKey standard encryption key
	 */
	private static final long serialVersionUID = 1L;
	
	private String propFileName = "properties"; //FIXME enter your path!
	private String paramName = "encud";
	private String baseKey = "keykey123";

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response)
		  throws ServletException, IOException {
	  
	PrintWriter out = response.getWriter();
	  
	String encud = request.getParameter(paramName);
	 
	byte[] crypt = HexConverter.HexToByteArray(encud); //Crypted data.
	byte[] dec = null; //encrypted data
	Crypter enc;
	
	
	ServletContext context = getServletContext(); 
	String fullPath = context.getRealPath(propFileName); //Real full magic path to property file. 
	
	PropertyReader propReader = new PropertyReader(fullPath);
	

	String key;
	
	try{ //Get key from property file or take base Key if some problem detected.
		if( (key = propReader.getKey()) == null){ 
			key = baseKey;
		}
	} catch (Exception e) {
		key= baseKey;
	}
	
	
	try {
		
		enc = new Crypter(key);
		dec = enc.decrypt(crypt); //Decrypting encrypted data.
	} catch (Exception e) {
		response.setContentType( "text/html" );  
		out.println("<H2>Ops!</H2></br>Some technical problems over there:(");
		e.printStackTrace();
		return;
	}
  
  
	String monoData = new String(dec, "UTF8" );//Unsplited data. Expecting userName_yyyyMMddHHmmss 
	String[] data = monoData.split("_"); 
 
	if(data.length < 2){
		response.setContentType( "text/html" );  
		out.println("<H2>Ops!</H2></br>Some technical problems over there:(</br> Invalid data.");
		return;
	}
  
	response.setContentType( "text/html" ); 
	out.println( "Username: " + data[0] + "</br>");
	out.println( "Date: " + TimeStamp.parse(data[1]) );
	return;
  }

}

