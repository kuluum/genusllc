package testPackage; 

import java.io.*;

import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.*;

/**
 * Redirect servlet.
 */
@WebServlet("/redirect") //URL path
public class Redirecter extends HttpServlet {

	/**
	 * @Field serialVersionUID pure magic.
	 * @Field propFileName path to file with properties
	 * @Field baseURL standard URL in case of invalid property file
	 * @Field baseKey standard decryption key in case of invalid property file
	 */
	private static final long serialVersionUID = 1L;
	
	private String propFileName = "properties"; //FIXME enter your path!
	
	private String baseURL = "http://survey.vts0.com/admin/login.cfm";
	private String baseKey = "keykey123";
	
  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
	  
	PrintWriter out = response.getWriter();
	  
	  /*
	   login just for valid getRemoteUser. 
	   FIXME Remove for release version.
	   
	   tomcat-users.xml:
	   <tomcat-users>
	   <user username="tomcat" password="tomcat" roles="tomcat"/>
	   ...
	   </tomcat-users>
	  */
	request.login("tomcat", "tomcat"); 
	  
	String currTime = TimeStamp.getTimeStamp();
	String user = request.getRemoteUser();
	  
	if(user == null){
		response.setContentType( "text/html" );  
		out.println("<H2>Sorry!</H2></br>You are not authorized.");
		return;
	}
	
	ServletContext context = getServletContext();
	String fullPath = context.getRealPath(propFileName); //Real full magic path to property file. 
	
	
	PropertyReader propReader = new PropertyReader(fullPath);

	String key = baseKey;
	String URL = baseURL;	
	
	
	try { //Get key from property file or take base Key if some problem detected.
		if( (key = propReader.getKey()) == null){ 
			key = baseKey;
		}
		if( (URL = propReader.getURL()) == null){ 
			URL = baseURL;
		}
	} catch (Exception e) {//Some problem with file. Dunow what to do, lol.
		e.printStackTrace();
	}
		
	String data = user + "_" + currTime; 
	  
	byte[]  encrypted = null;
	try {
		Crypter crypter = new Crypter(key); 
		encrypted = crypter.encrypt(data.getBytes()); //Encryption incoming data.
	} catch (Exception e1) {
		response.setContentType( "text/html" );  
		out.println("<H2>Ops!</H2></br>Some technical problems over there:(");
		e1.printStackTrace();
		return;
	}
	  
	String hexEncryptData = HexConverter.byteArrayToHex(encrypted); //Formating encrypted data to hex string.
	  
	
	
	//FIXME Uncomment for release version. 
	//--------
	String forwardURL =  URL + '?' + "encud=" + hexEncryptData;
	response.sendRedirect(forwardURL);
	//---------

	//FIXME Decryption_test check version. Remove for release version.
	//------
	//String tmp = "http://localhost:8080"+ request.getContextPath() + "/decryptiontest?encud=" + hexEncryptData;
	//response.sendRedirect(tmp);
	//------
	  
	  
  }

}
